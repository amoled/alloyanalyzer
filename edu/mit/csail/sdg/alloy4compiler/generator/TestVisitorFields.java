package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4compiler.ast.Type;
import edu.mit.csail.sdg.alloy4.Err;
//import edu.mit.csail.sdg.alloy4compiler.ast.Expr;
//import edu.mit.csail.sdg.alloy4compiler.ast.ExprBinary;
//import edu.mit.csail.sdg.alloy4compiler.ast.ExprCall;
//import edu.mit.csail.sdg.alloy4compiler.ast.ExprConstant;
//import edu.mit.csail.sdg.alloy4compiler.ast.ExprCustom;
//import edu.mit.csail.sdg.alloy4compiler.ast.ExprHasName;
//import edu.mit.csail.sdg.alloy4compiler.ast.ExprList;
//import edu.mit.csail.sdg.alloy4compiler.ast.ExprQt;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprUnary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprVar;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.ast.VisitQuery;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.Field;

public class TestVisitorFields extends VisitQuery<String> {

	@Override
	public String visit(Sig sig) throws Err {
		if (sig.label.startsWith("this/"))
			return sig.label.substring(5);
		return sig.label;
	}

	@Override
	public String visit(Field f) throws Err {
		return f.decl().expr.accept(this);
	}

	public String visit(Type typ) throws Err {
		return typ.toString().substring(6).replace("}", "");
	}

	@Override
	public String visit(ExprVar expr) throws Err {		
		return expr.label.replace("$", "");
	}
	
	
	@Override
	public String visit(ExprUnary expr) throws Err { // evtl. noch rclosure, closure, und noof handeln!?
		if (expr.op == ExprUnary.Op.SETOF) {
			return "setof";//" = " + expr.type().toString() + expr.sub.accept(this) + expr.sub.getClass() + ".Add(";
		} else if (expr.op == ExprUnary.Op.ONEOF) {
			return "oneof"; //" = " + expr.type().toString() + expr.sub.accept(this) + expr.sub.getClass();
		} else if (expr.op == ExprUnary.Op.LONEOF) {
			return "loneof";
		} else if (expr.op == ExprUnary.Op.SOMEOF) {
			return "someof";
		} else if (expr.op == ExprUnary.Op.NOOP) {
			return expr.sub.accept(this);
		}
		return super.visit(expr);
	}	
	
	
//	public String expr(Expr expr) throws Err {
//		if (expr instanceof ExprCall) {
//			 return "ExrpCall"; 
//		  }
//		  if (expr instanceof ExprConstant) {
//			  return "ExprConstant"; 
//		  }
//		  if (expr instanceof ExprCustom) {
//			  return "ExprCustom"; 
//		  }
//		  if (expr instanceof ExprHasName) {
//			  return "ExprHasName"; 
//		  }
//		  if (expr instanceof ExprUnary) {
//			  return "ExprUnary";
//		  }
//		  if (expr instanceof ExprBinary) {
//			  return "ExprBinary"; 
//		  }
//		  if (expr instanceof ExprList) {
//			  return "ExprList"; 
//		  }
//		  if (expr instanceof ExprQt) {
//			  return "ExprQt"; 
//		  }
//		  return "Error expr";
//	}
}
