package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.Expr;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprBinary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprConstant;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprUnary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprVar;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.PrimSig;
import edu.mit.csail.sdg.alloy4compiler.ast.VisitQuery;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.Field;

public class VisitorDecl extends VisitQuery<String> {

	
	public VisitorDecl() {
		super();
	}
	
	@Override
	public String visit(Field f) throws Err {
		///remove it's ISet... Only want it's type.
		if (f.decl().expr instanceof ExprUnary) {
			Expr e = f.decl().expr;
			while (e instanceof ExprUnary) {
				e = ((ExprUnary) e).sub;
			}
			return e.accept(this);
		} else {
			return f.decl().expr.accept(this);	//I don't want it's name but it's type.
		}
	}

	@Override
	public String visit(Sig sig) throws Err {
		
		topSig[0] = sig;
		if (!sig.builtin) {
			return sig.label.substring(5);
		} else if (sig == Sig.UNIV) {
			return "Object";
		} else if (sig == Sig.SIGINT){
			return "int";
		} 
		return sig.label;
	}
	
	@Override
	public String visit(ExprVar expr) throws Err {
		return expr.type().toExpr().accept(this);
	}
	
	@Override
	public String visit(ExprUnary expr) throws Err {
		String sub = expr.sub.accept(this);
		if (expr.op == ExprUnary.Op.SOMEOF) {
			return "ISet<" + sub + ">";
		} else if (expr.op == ExprUnary.Op.SETOF) {
			return "ISet<" + sub + ">";
		} else if (expr.op == ExprUnary.Op.CARDINALITY) {
			topSig[0] = Sig.SIGINT;
		} else if (expr.op == ExprUnary.Op.TRANSPOSE) {
			if (hasTwo) {
				Sig tmp = topSig[0];
				topSig[0] = topSig[1];
				topSig[1] = tmp;
				sub = "ISet<" + topSig() + ">";
			}
		} else if (expr.op == ExprUnary.Op.CAST2INT) {
			sub = "int";
		} else if (expr.op == ExprUnary.Op.CAST2SIGINT) {
			sub = "int?";
		}
		
		return sub;
	}
	
	@Override
	public String visit(ExprConstant expr) throws Err {
		if(expr.op == ExprConstant.Op.FALSE){
			return "false";
		} else if (expr.op == ExprConstant.Op.TRUE) {
			return "true";
		} else if (expr.op == ExprConstant.Op.MAX) {
			return "Int64.MaxValue";
		} else if (expr.op == ExprConstant.Op.MIN) {
			return "Int64.MinValue";
		} else if (expr.op == ExprConstant.Op.NUMBER) {
			return expr.num + "";
		} else {
			return expr.op.toString();
		}
	}
	
	boolean hasTwo = false;
	
	private Sig[] topSig = new Sig[2];
	
	private Sig[] leftSig = new Sig[2];
	
	private Sig[] rightSig = new Sig[2];
	
	public String leftSig() {
		if (!hasTwo) {
			if (leftSig[0] == Sig.UNIV || leftSig[0] == null)
				return "Object";
			else if (leftSig[0] == Sig.SIGINT)
				return "int";
			else
				return leftSig[0].label.replace("this/", "");
		} else {
			String first;
			String second;
			
			if (leftSig[0] == Sig.UNIV || leftSig[0] == null)
				first = "Object";
			else if (leftSig[0] == Sig.SIGINT)
				first = "int";
			else
				first = leftSig[0].label.replace("this/", "");
			
			if (leftSig[1] == Sig.UNIV)
				second = "Object";
			else if (leftSig[1] == Sig.SIGINT)
				second = "int";
			else if (leftSig[1] == null)
				second = "";
			else
				second = leftSig[1].label.replace("this/", "");
			
			return "Tuple<" + first + ", " + second + ">";
		}
	}
	
	public String rightSig() {
		if (!hasTwo) {
			if (rightSig[0] == Sig.UNIV || rightSig[0] == null)
				return "Object";
			else if (rightSig[0] == Sig.SIGINT)
				return "int";
			else
				return rightSig[0].label.replace("this/", "");
		} else {
			String first;
			String second;
			
			if (rightSig[0] == Sig.UNIV || rightSig[0] == null)
				first = "Object";
			else if (rightSig[0] == Sig.SIGINT)
				first = "int";
			else
				first = rightSig[0].label.replace("this/", "");
			
			if (rightSig[1] == Sig.UNIV)
				second = "Object";
			else if (rightSig[1] == Sig.SIGINT)
				second = "int";
			else if (rightSig[1] == null)
				second = "";
			else
				second = rightSig[1].label.replace("this/", "");
			
			return "Tuple<" + first + ", " + second + ">";
		}
	}
	
	public String topSig() {
		if (!hasTwo) {
			if (topSig[0] == Sig.UNIV || topSig[0] == null)
				return "Object";
			else if (topSig[0] == Sig.SIGINT)
				return "int";
			else
				return topSig[0].label.replace("this/", "");
		} else {
			String first;
			String second;
			
			if (topSig[0] == Sig.UNIV || topSig[0] == null)
				first = "Object";
			else if (topSig[0] == Sig.SIGINT)
				first = "int";
			else
				first = topSig[0].label.replace("this/", "");
			
			if (topSig[1] == Sig.UNIV)
				second = "Object";
			else if (topSig[1] == Sig.SIGINT)
				second = "int";
			else if (topSig[1] == null)
				second = "Object";
			else
				second = topSig[1].label.replace("this/", "");
			
			return "Tuple<" + first + ", " + second + ">";
		}
	}

	@Override
	public String visit(ExprBinary expr) throws Err {
		 String out = "";
		 
		 boolean hasTwoSave = this.hasTwo;
		 
		 String left = expr.left.accept(this);
		 Sig[] leftSig = topSig.clone();

		 
		 this.hasTwo = hasTwoSave;
		 
		 String right = expr.right.accept(this);
		 Sig[] rightSig = topSig.clone();
		 
		 this.leftSig = leftSig;
		 this.rightSig = rightSig;
		 
		 if (expr.op == ExprBinary.Op.SOME_ARROW_SOME) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;//cannot be true before - else we would get arities bigger than 2.
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.ANY_ARROW_SOME) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.SOME_ARROW_ANY) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.ANY_ARROW_ONE) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.ANY_ARROW_LONE) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.ONE_ARROW_ANY) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.ONE_ARROW_ONE) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.ONE_ARROW_LONE) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.ONE_ARROW_SOME) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.SOME_ARROW_LONE) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.SOME_ARROW_ONE) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.LONE_ARROW_ANY) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.LONE_ARROW_LONE) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.LONE_ARROW_ONE) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.LONE_ARROW_SOME) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.ARROW) {
			 out = "ISet<Tuple<" + left + ", " + right + ">>";
			 hasTwo = true;
			 topSig[0] = leftSig[0];
			 topSig[1] = rightSig[0];
		 } else if (expr.op == ExprBinary.Op.DOMAIN) {
			 out = right;
			 topSig = rightSig;
		 } else if (expr.op == ExprBinary.Op.RANGE) {
			 out = left;
			 topSig = leftSig;
		 } else if (expr.op == ExprBinary.Op.JOIN){
			 out = right;
			 topSig = rightSig;
		 } else {
			 if(!(hasTwo) && leftSig[0] instanceof PrimSig && rightSig[0] instanceof PrimSig) {
				 topSig[0] = ((PrimSig) leftSig[0]).leastParent((PrimSig) rightSig[0]);
			 } else if (!(hasTwo)) {
				 topSig[0] = Sig.UNIV;
			 } else {	//hasTwo
				 if (leftSig[0] instanceof PrimSig && rightSig[0] instanceof PrimSig)
					 topSig[0] = ((PrimSig) leftSig[0]).leastParent((PrimSig) rightSig[0]);
				 else
					 topSig[0] = Sig.UNIV;
				 
				 if (leftSig[1] instanceof PrimSig && rightSig[1] instanceof PrimSig)
					 topSig[1] = ((PrimSig) leftSig[1]).leastParent((PrimSig) rightSig[1]);
				 else
					 topSig[1] = Sig.UNIV;
			 }
			 
			 out = topSig();
			 
		 }
		  
		  return out;
	}
}
