package edu.mit.csail.sdg.alloy4compiler.generator;

import java.util.ArrayList;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprBinary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprCall;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprConstant;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprList;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprUnary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprVar;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.ast.VisitQuery;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.Field;

public class Visitor extends VisitQuery<String> {
	
	//the Sets are this type.
	public String defs = "";
	
	//the delegates I need. Just Copy from here.
	public ArrayList<String> delsNeeded= new ArrayList<String>();
	
	int i = 0;
	
	@Override
	public String visit(ExprCall x) throws Err {
		String out = x.fun.label.substring(5) + "(";
		
		for(int i=0;i < x.args.size();i++) {
			out += x.args.get(i).accept(this);
			if (i < x.args.size() - 1)
				out += ", ";
			else
				out += ")";
		}
		return out;
	}
	
	@Override
	public String visit(ExprList list) throws Err {
		String out = "( ";
		for(int i=0;i < list.args.size();i++) {
        	out += list.args.get(i).accept(this);
			if (i < list.args.size() - 1)
				
				if (list.op == ExprList.Op.AND) {
					out += " && ";
				} else if (list.op == ExprList.Op.OR) {
					out += " || ";
				} else {
					out += " listOpNotSupported ";
				}
			
        }
        return out + " )";
	}
	
	@Override
	public String visit(Field f) throws Err {
		return f.label;
	}

	@Override
	public String visit(Sig sig) throws Err {
		return sig.label.substring(5);
	}
	
	@Override
	public String visit(ExprVar expr) throws Err {
		return expr.label;
	}
	
	@Override
	public String visit(ExprUnary expr) throws Err {
		int i = this.i;
		
		this.i++;
		String subString = expr.sub.accept(this);
		VisitorDecl vis = new VisitorDecl();
		
		String inDef = expr.sub.accept(vis);
		
		if (expr.op == ExprUnary.Op.CLOSURE) {
			return "Helper.Closure(" + subString + ")";
			
		} else if (expr.op == ExprUnary.Op.RCLOSURE) {
			return "Helper.RClosure(" + subString + ")";
			
		} else if (expr.op == ExprUnary.Op.NOT) {
			return "!" + subString;
			
		} else if (expr.op == ExprUnary.Op.CARDINALITY) {
			  defs += String.format("\tvar sub%d = %s;\n",i, subString);
			  
			  String tmp = "\tIntDel delCardinality%1$d = () => {\n"
				  		+ "\t\tif (sub%1$d is ISet<%2$s>)\n"
				  		+ "\t\t\treturn ((ISet<%2$s>) sub%1$d).Count();\n"
				  		+ "\t\telse\n"
				  		+ "\t\t\treturn 1;\n"
				  		+ "\t};\n\n";

			  defs += String.format(tmp, i, inDef);
			  return String.format("delCardinality%d()",i);
			
		} else if (expr.op == ExprUnary.Op.CAST2INT) {
			return "(int) " + subString;
			
		} else if (expr.op == ExprUnary.Op.CAST2SIGINT) {
			return "(int) " + subString;
			
		} else if (expr.op == ExprUnary.Op.LONEOF) {
			return subString + ".Single()";
			
		} else if (expr.op == ExprUnary.Op.ONEOF) {
			return subString + ".Single()";
			
		} else if (expr.op == ExprUnary.Op.SETOF) {
			return "ISet< "+ subString + ">";
			
		} else if (expr.op == ExprUnary.Op.SOMEOF) {
			return "ISet< "+ subString + ">";
			
		} else if (expr.op == ExprUnary.Op.TRANSPOSE) {
			//returnDef = "Tuple<" + vis.rightSig().replace("this/", "") + ", " + vis.leftSig().replace("this/", "") + ">";
			
			//String inDef = "Tuple<" + vis.leftSig().replace("this/", "") + ", " + vis.rightSig().replace("this/", "") + ">";
			
			String outDef = expr.accept(vis);
			
			
			String name = outDef.replace("<", "").replace(">", "").replace(" ", "").replace(",", "");
			
			delsNeeded.add("  delegate ISet<" + outDef + "> SetDelTr" + name + "();\n");
		    
			defs += String.format("\tvar sub%d = %s;\n",i, subString);
		    String tmp = "\tSetDelTr%2$s delTranspose%1$d = () => {\n"
		    			+ "\t\tISet<%3$s> set = new HashSet<%3$s>();\n"
		    			+ "\t\tforeach (%4$s t in (ISet<%4$s>) sub%1$d)\n"
		    			+ "\t\t\tset.Add(new %3$s(t.Item2, t.Item1));\n"
		    			+ "\t\treturn set;\n"
		    			+ "\t};\n\n";

		    defs += String.format( tmp , i, name, outDef, inDef);
		
		    return String.format("delTranspose%d()",i);
			
		}
		
		return subString;
	}
	
	@Override
	public String visit(ExprConstant expr) throws Err {
		if(expr.op == ExprConstant.Op.FALSE){
			return "false";
		} else if (expr.op == ExprConstant.Op.TRUE) {
			return "true";
		} else if (expr.op == ExprConstant.Op.MAX) {
			return "Int.MaxValue";
		} else if (expr.op == ExprConstant.Op.MIN) {
			return "Int.MinValue";
		} else if (expr.op == ExprConstant.Op.NUMBER) {
			return expr.num + "";
		} else if (expr.op == ExprConstant.Op.STRING) {
			return expr.string;
		} else {
			return expr.op.toString();
		}
	}
	
	@Override
	public String visit(ExprBinary expr) throws Err {
		  String out;		 
		  int i = this.i;
		 
		  this.i++;
		  String left = expr.left.accept(this);
		  
		  this.i++;
		  String right = expr.right.accept(this);
		  
		  //set the return type and the respective left and right type.
		  VisitorDecl vis = new VisitorDecl();
		  
		  String returnDef = expr.accept(vis);
		  
		  String nameDel = returnDef.replace("<", "").replace(">", "").replace(" ", "").replace(",", "");
		  
		  String leftDef = vis.leftSig().replace("this/", "");
		  String rightDef = vis.rightSig().replace("this/", "");
		  
		  if (leftDef.equals("Int")) {
			  leftDef = "int";
		  }
		  
		  if (rightDef.equals("Int")) {
			  rightDef = "int";
		  }
		  
		  if (expr.op == ExprBinary.Op.AND) {
			  out = "(" + left + " && " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.OR) {
			  out = "(" + left + " || " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.MUL) {
			  out = "(" + left + " * " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.DIV) {
			  out = "(" + left + " / " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.EQUALS) {
			  //==?
			  out = "(" + left + ".Equals(" + right + "))";
			  
		  } else if (expr.op == ExprBinary.Op.GT) {
			  out = "(" + left + " > " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.GTE) {
			  out = "(" + left + " >= " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.IFF) {
			  out = "(" + left + " == " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.IMINUS) {
			  out = "(" + left + " - " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.IMPLIES) {
			  out = "(!(" + left + ") || " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.IN) {
			  defs += String.format("\tvar l%d = %s;\n",i,left);
			  defs += String.format("\tvar r%d = %s;\n",i,right);

			  String tmp = "\tBoolDel delIn%1$d = () => {\n"
					  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
					  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
					  	
					  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
					  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
					  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
					  	+ "\t\t}\n"
					  	
					  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
					  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
					  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
					  	+ "\t\t}\n"
					  	
				  		+ "\t\treturn setLeft.IsSubsetOf(setRight);\n"
				  		+ "\t};\n\n";
			  
			  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef);
			  out = String.format("delIn%d()",i);
			  
		  } else if (expr.op == ExprBinary.Op.INTERSECT) {
			  delsNeeded.add("  delegate ISet<" + returnDef + "> SetDel" + nameDel + "();\n");
			  
			  defs += String.format("\tvar l%d = %s;\n",i,left);
			  defs += String.format("\tvar r%d = %s;\n",i,right);

			  String tmp = "\tSetDel%6$s delIntersect%1$d = () => {\n"
					  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
					  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
					  	
					  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
					  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
					  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
					  	+ "\t\t}\n"
					  	
					  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
					  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
					  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
					  	+ "\t\t}\n"
					  	
				  		+ "\t\tsetLeft.IntersectWith(setRight);\n"
				  		+ "\t\treturn setLeft;\n"

				  		+ "\t};\n\n";
			  
			  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef, nameDel);

			  out = String.format("delIntersect%d()",i);
		  } else if (expr.op == ExprBinary.Op.IPLUS) {
			  out = "(" + left + " + " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.JOIN) {
			  out = "(" + left +  "." + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.LT) {
			  out = "(" + left + " < " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.LTE) {
			  out = "(" + left + " =< " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.MINUS) {
			  if (returnDef.equals("int") && returnDef.equals("int") && returnDef.equals("int")) {
				  out = "(" + left + " - " + right + ")";
			  } else {
				  delsNeeded.add("  delegate ISet<" + returnDef + "> SetDel" + nameDel + "();\n");
				  
				  defs += String.format("\tvar l%d = %s;\n",i,left);
				  defs += String.format("\tvar r%d = %s;\n",i,right);
				  String tmp = "\tSetDel%6$s delMinus%1$d = () => {\n"
						  
						  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
						  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
						  	
						  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
						  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
						  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
						  	+ "\t\t} else {\n"
						  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
						  	+ "\t\t}\n"
						  	
						  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
						  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
						  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
						  	+ "\t\t} else {\n"
						  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
						  	+ "\t\t}\n"
						  	
					  		+ "\t\tsetLeft.ExceptWith(setRight);\n"
					  		+ "\t\treturn setLeft;\n"
	
					  		+ "\t};\n\n";
				  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef, nameDel);
				  out = String.format("delMinus%d()",i);
			  }
		  } else if (expr.op == ExprBinary.Op.NOT_EQUALS) {
			  out = "!(" + left + ".Equals( " + right + "))";
			  
		  } else if (expr.op == ExprBinary.Op.NOT_GT) {
			  out = "(" + left + " =< " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.NOT_GTE) {
			  out = "(" + left + " < " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.NOT_IN) {
			  defs += String.format("\tvar l%d = %s;\n",i,left);
			  defs += String.format("\tvar r%d = %s;\n",i,right);

			  String tmp = "\tBoolDel delNotIn%1$d = () => {\n"
					  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
					  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
					  	
					  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
					  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
					  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
					  	+ "\t\t}\n"
					  	
					  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
					  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
					  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
					  	+ "\t\t}\n"
					  	
				  		+ "\t\treturn !(setLeft.IsSubsetOf(setRight));\n"
				  		+ "\t};\n\n";
			  
			  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef);
			  out = String.format("delNotIn%d()",i);
			  
		  } else if (expr.op == ExprBinary.Op.NOT_LT) {
			  out = "(" + left + " >= " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.NOT_LTE) {
			  out = "(" + left + " > " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.PLUS) {
			  if (returnDef.equals("int") && returnDef.equals("int") && returnDef.equals("int")) {
				  out = "(" + left + " - " + right + ")";
			  } else {
				  delsNeeded.add("  delegate ISet<" + returnDef + "> SetDel" + nameDel + "();\n");
				  
				  defs += String.format("\tvar l%d = %s;\n",i,left);
				  defs += String.format("\tvar r%d = %s;\n",i,right);
	
				  String tmp = "\tSetDel%6$s delPlus%1$d = () => {\n"
						  
						  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
						  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
						  	
						  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
						  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
						  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
						  	+ "\t\t} else {\n"
						  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
						  	+ "\t\t}\n"
						  	
						  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
						  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
						  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
						  	+ "\t\t} else {\n"
						  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
						  	+ "\t\t}\n"
						  
					  		+ "\t\tsetLeft.UnionWith(setRight);\n"
					  		+ "\t\treturn setLeft;\n"
					  		
					  		+ "\t};\n\n";
				  
				  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef, nameDel);
	
				  out = String.format("delPlus%d()",i);
			  }
		  } else if (expr.op == ExprBinary.Op.REM) {
			  out = "(" + left + " % " + right + ")";
			  
		  } else {
			  out = "(" + left + expr.op + right + ")";
			  
		  }
		  return out;
		  
	}
}
