package edu.mit.csail.sdg.alloy4compiler.generator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprBinary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprConstant;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprUnary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprVar;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.ast.VisitQuery;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.Field;

public class VisitorInv extends VisitQuery<String> {
	
	int i = 0;
	
	@Override
	public String visit(Field f) throws Err {
		return f.label + ".Equals(%s)";
	}

	@Override
	public String visit(Sig sig) throws Err {
		return "";
	}
	
	@Override
	public String visit(ExprVar expr) throws Err {
		return "";
	}
	
	@Override
	public String visit(ExprUnary expr) throws Err {
		int i = this.i;
		
		this.i++;
		String subString = expr.sub.accept(this);
		
		if (expr.op == ExprUnary.Op.SOMEOF) {
			String out = String.format("e%1$d => e%1$d != null", i);
			
			if (!subString.isEmpty())
				out += " && " + String.format(subString, "e" + i);
			
			return "Contract.ForAll(%s," + out + " )";
			
		} else if (expr.op == ExprUnary.Op.SETOF) {
			String out = String.format("e%1$d => e%1$d != null", i);
			
			if (!subString.isEmpty())
				out += " && " + String.format(subString, "e" + i);
			
			return "Contract.ForAll(%s," + out + " )";
		}
		
		return subString;
	}
	
	@Override
	public String visit(ExprConstant expr) throws Err {
		return "";
	}
	
	@Override
	public String visit(ExprBinary expr) throws Err {
		 String out = "";
		 int i = this.i;
		 
		 String myVar = String.format("e%d", i);
		 
		 this.i++;
		 String left = expr.left.accept(this);
		 
		 this.i++;
		 String right = expr.right.accept(this);
		 
		 
		 if (expr.op == ExprBinary.Op.SOME_ARROW_SOME) {
			 out = String.format("%1$s => %1$s != null && %1$s.Item1 != null && %1$s.Item2 != null",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.SOME_ARROW_ANY) {
			 out = String.format("%1$s => %1$s != null && %1$s.Item1 != null && %1$s.Item2 != null",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.SOME_ARROW_LONE) {
			 out = String.format("%1$s => %1$s != null && %1$s.Item1 != null && %1$s.Item2 != null",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.SOME_ARROW_ONE) {
			 out = String.format("%1$s => %1$s.Item1 != null && %1$s.Item2 != null ",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out += " && Contract.ForAll(%s, r => r.Item2 == " + myVar + ".Item2)";
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.ONE_ARROW_LONE) {
			 out = String.format("%1$s => %1$s.Item1 != null && %1$s.Item2 != null ",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out += " && Contract.ForAll(%1$s, l => l.Item1 == " + myVar + ".Item1)";
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.ONE_ARROW_ONE) {
			 out = String.format("%1$s => %1$s.Item1 != null && %1$s.Item2 != null ",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out += " && Contract.ForAll(%1$s, l => l.Item1 == " + myVar + ".Item1)";
			 out += " && Contract.ForAll(%1$s, r => r.Item2 == " + myVar + ".Item2)";
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.ONE_ARROW_SOME) {
			 out = String.format("%1$s => %1$s != null && %1$s.Item1 != null && %1$s.Item2 != null",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out += " && Contract.ForAll(%1$s, l => l.Item1 == " + myVar + ".Item1)";
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.ONE_ARROW_ANY) {
			 out = String.format("%1$s => %1$s != null && %1$s.Item1 != null && %1$s.Item2 != null",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out += " && Contract.ForAll(%1$s, l => l.Item1 == " + myVar + ".Item1)";
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 }else if (expr.op == ExprBinary.Op.LONE_ARROW_ANY) {
			 out = String.format("%1$s => %1$s != null && %1$s.Item1 != null && %1$s.Item2 != null",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.LONE_ARROW_LONE) {
			 out = String.format("%1$s => %1$s != null && %1$s.Item1 != null && %1$s.Item2 != null",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.LONE_ARROW_ONE) {
			 out = String.format("%1$s => %1$s != null && %1$s.Item1 != null && %1$s.Item2 != null",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out += " && Contract.ForAll(%1$s, r => r.Item2 == " + myVar + ".Item2)";
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.LONE_ARROW_SOME) {
			 out = String.format("%1$s => %1$s != null && %1$s.Item1 != null && %1$s.Item2 != null",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.ANY_ARROW_LONE) {
			 out = String.format("%1$s => %1$s != null && %1$s.Item1 != null && %1$s.Item2 != null",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.ANY_ARROW_ONE) {
			 out = String.format("%1$s => %1$s.Item1 != null && %1$s.Item2 != null ",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out += " && Contract.ForAll(%1$s, r => r.Item2 == " + myVar + ".Item2)";
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.ANY_ARROW_SOME) {
			 out = String.format("%1$s => %1$s != null && %1$s.Item1 != null && %1$s.Item2 != null",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } else if (expr.op == ExprBinary.Op.ARROW) {
			 out = String.format("%1$s => %1$s != null && %1$s.Item1 != null && %1$s.Item2 != null",myVar);
			 if (!left.isEmpty()) {
				 out += " && " + String.format(left, myVar + ".Item1");
			 }
			 if (!right.isEmpty()) {
				 out += " && " + String.format(right, myVar + ".Item2");
			 }
			 out = "Contract.ForAll(%1$s," + out + ")";
			 
		 } //else if (expr.op == ExprBinary.Op.DOMAIN) {
			 //out = right + ".Equals(%s)";}
		 else if (expr.op == ExprBinary.Op.JOIN){
			 out = right;
		 }
		 
		 return out;
	}
}
