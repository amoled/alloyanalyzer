package edu.mit.csail.sdg.alloy4compiler.generator;

import java.util.ArrayList;

import edu.mit.csail.sdg.alloy4compiler.ast.Type;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprBinary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprCall;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprConstant;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprList;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprQt;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprUnary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprVar;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.ast.VisitQuery;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.Field;

public class TestVisitor extends VisitQuery<String> {
	
	//the Sets are this type.
	public String defs = "";
	
	//the delegates I need. Just Copy from here.
	public ArrayList<String> delsNeeded= new ArrayList<String>();
	
	int i = 0;
	
	
	@Override
	public String visit(Sig sig) throws Err {
		return sig.label.replace("this/", "").replace("seq/", "").replace("boolean/", "");
	}

	@Override
	public String visit(Field f) throws Err {
		return f.label;
	}

	public String visit(Type typ) throws Err {
		return typ.toString().substring(6).replace("}", "").replace("an/", "");
	}

	@Override
	public String visit(ExprVar expr) throws Err {		
		return expr.label.replace("$", "").replace("boolean/", "").replace("'", "prime");
	}
			
	public String clean(String s) throws Err {
		s = s.replace("$", "");		
		return s;
	}	
	
	@Override public String visit(ExprCall x) throws Err {
    	
    	// The answer-string
    	String ans = "";
    	
    	ans = "FuncClass." + x.fun.label.substring(5) + "(";
    	int s = x.args.size();
    	if (s > 1) {
    		for (int i = 0; i < s-1; i++) {
    			ans = ans + x.args.get(i).accept(this) + ", ";
    		}
    		ans = ans + x.args.get(s-1).accept(this) + ")";
    	} else if (s == 1) {
    		ans = ans + x.args.get(0).accept(this) + ")";
    	} else {
    		ans = ans + ")";
    	}
    	return ans;    	
    }	
	
	@Override
	public String visit(ExprConstant expr) throws Err {
		if(expr.op == ExprConstant.Op.FALSE){
			return "false";
		} else if (expr.op == ExprConstant.Op.TRUE) {
			return "true";
		} else if (expr.op == ExprConstant.Op.MAX) {
			return "Int.MaxValue";
		} else if (expr.op == ExprConstant.Op.MIN) {
			return "Int.MinValue";
		} else if (expr.op == ExprConstant.Op.NUMBER) {
			return expr.num + "";
		} else if (expr.op == ExprConstant.Op.STRING) {
			return expr.string;
		} else {
			return expr.op.toString();
		}
	}
	
	
	@Override
	public String visit(ExprList list) throws Err {
		String out = "( ";
		for(int i=0;i < list.args.size();i++) {
        	out += list.args.get(i).accept(this);
			if (i < list.args.size() - 1)
				
				if (list.op == ExprList.Op.AND) {
					out += " && ";
				} else if (list.op == ExprList.Op.OR) {
					out += " || ";
				} else {
					out += " listOpNotSupported ";
				}
			
        }
        return out + " )";
	}
	
	@Override
	public String visit(ExprUnary expr) throws Err {
		int i = this.i;
		
		this.i++;
		String subString = expr.sub.accept(this);
		VisitorDecl vis = new VisitorDecl();
		
		String inDef = expr.sub.accept(vis);
		
		if (expr.op == ExprUnary.Op.NOOP) {
			return subString;
			
		} else if (expr.op == ExprUnary.Op.CLOSURE) {
			return "Helper.Closure(" + subString + ")";
			
		} else if (expr.op == ExprUnary.Op.RCLOSURE) {
			return "Helper.RClosure(" + subString + ")";
			
		} else if (expr.op == ExprUnary.Op.NOT) {
			return "!" + subString;
			
		} else if (expr.op == ExprUnary.Op.CARDINALITY) {
			
			delsNeeded.add("  delegate int IntDel" + i + "(" 
					  + this.visit(expr.sub.type()) 
					  + " a" + i + ");\n");
						  
			  String tmp = "\tIntDel" + i + " delCardinality%1$d = (a" + i + ") => {\n"
					    + "\t" + String.format("\tvar sub%d = %s;\n",i, "a" + i)
				  		+ "\t\tif (sub%1$d is ISet<%2$s>)\n"
				  		+ "\t\t\treturn ((ISet<%2$s>) sub%1$d).Count();\n"
				  		+ "\t\telse\n"
				  		+ "\t\t\treturn 1;\n"
				  		+ "\t};\n\n";
			  

			  defs += String.format(tmp, i, inDef);
			  return String.format("delCardinality%d(" + subString + ")",i);
			
		} else if (expr.op == ExprUnary.Op.CAST2INT) {
			return "(int) " + subString;
			
		} else if (expr.op == ExprUnary.Op.CAST2SIGINT) {
			return "(int) " + subString;
			
		} else if (expr.op == ExprUnary.Op.LONEOF) {
			return subString;
			
		} else if (expr.op == ExprUnary.Op.ONEOF) {
			return subString;
			
		} else if (expr.op == ExprUnary.Op.SETOF) {
			return "ISet< "+ subString + ">";
			
		} else if (expr.op == ExprUnary.Op.SOMEOF) {
			return "ISet< "+ subString + ">";
			
		} else if (expr.op == ExprUnary.Op.TRANSPOSE) {
			//returnDef = "Tuple<" + vis.rightSig().replace("this/", "") + ", " + vis.leftSig().replace("this/", "") + ">";
			
			//String inDef = "Tuple<" + vis.leftSig().replace("this/", "") + ", " + vis.rightSig().replace("this/", "") + ">";
			
			String outDef = expr.accept(vis);
			
			
			String name = outDef.replace("<", "").replace(">", "").replace(" ", "").replace(",", "");
			
			delsNeeded.add("  delegate ISet<" + outDef + "> SetDelTr" + name + i 
					+ "(" + this.visit(expr.sub.type()) + " a" + i + ");\n");
		   
						
		    String tmp = "\tSetDelTr%2$s" + i + " delTranspose%1$d = (a" + i + ") => {\n"
		    			+ "\t" + String.format("\tvar sub%d = %s;\n",i, "a" + i)
		    			+ "\t\tISet<%3$s> set = new HashSet<%3$s>();\n"
		    			+ "\t\tforeach (%4$s t in (ISet<%4$s>) sub%1$d)\n"
		    			+ "\t\t\tset.Add(new %3$s(t.Item2, t.Item1));\n"
		    			+ "\t\treturn set;\n"
		    			+ "\t};\n\n";

		    defs += String.format( tmp , i, name, outDef, inDef);
		
		    return String.format("delTranspose%d(" + subString + ")",i);
			
		} 
		
		return subString;
	}
	
    @Override
    public String visit(ExprQt x) throws Err {
    	
    	// The answer-strings
    	String left = "";
    	String right = "";
    	String sub = x.sub.accept(this);
    	
    	int aT = x.decls.size(); // Anzahl Typen
    	
    	if (x.op == ExprQt.Op.ALL) {
    		for (int i = 0; i < aT; i++) { // iterate over types
        		int aV = x.decls.get(i).names.size();
        		for (int j = 0; j < aV; j++) { // iterates over variables        			
        			left = left + "Contract.ForAll(" + x.decls.get(i).expr.accept(this) + "Set, " + x.decls.get(i).names.get(j).toString().replace("'", "prime") + " => "; 
        			right = ")" + right;
        		}
        	}
		} else if (x.op == ExprQt.Op.ONE) {
			for (int i = 0; i < aT; i++) { // iterate over types
	    		int aV = x.decls.get(i).names.size();
	    		for (int j = 0; j < aV; j++) { // iterates over variables
	    			left = left + x.decls.get(i).expr.accept(this) + "Set.Where(" + x.decls.get(i).names.get(j).toString().replace("'", "prime") + " => ";
	    			right = ").Count() == 1" + right;	    			
	    		}
	    	}
		} else if (x.op == ExprQt.Op.NO) {
			for (int i = 0; i < aT; i++) { // iterate over types
	    		int aV = x.decls.get(i).names.size();
	    		for (int j = 0; j < aV; j++) { // iterates over variables
	    			left = left + "Contract.ForAll(" + x.decls.get(i).expr.accept(this) + "Set, " + x.decls.get(i).names.get(j).toString().replace("'", "prime") + " => !";
	    			right = ")" + right;	    			
	    		}
	    	}
		} else if (x.op == ExprQt.Op.SOME) {
			for (int i = 0; i < aT; i++) { // iterate over types
	    		int aV = x.decls.get(i).names.size();
	    		for (int j = 0; j < aV; j++) { // iterates over variables
	    			left = left + "Contract.Exists(" + x.decls.get(i).expr.accept(this) + "Set, " + x.decls.get(i).names.get(j).toString().replace("'", "prime") + " => ";
	    			right = ")" + right;	    			
	    		}
	    	}
		} else if (x.op == ExprQt.Op.LONE) {
			for (int i = 0; i < aT; i++) { // iterate over types
	    		int aV = x.decls.get(i).names.size();
	    		for (int j = 0; j < aV; j++) { // iterates over variables
	    			left = left + x.decls.get(i).expr.accept(this) + "Set.Where(" + x.decls.get(i).names.get(j).toString().replace("'", "prime") + " => ";
	    			right = ").Count() <= 1" + right;
	    		}
	    	}
			
		} else {
			// Quantifier we don't have to support!
		}
    	    	
    	return left + sub + right;
    }
    
    @Override
	public String visit(ExprBinary expr) throws Err {
		  String out = "";		 
		  int i = this.i;
		 
		  this.i++;
		  String left = expr.left.accept(this);
		  
		  this.i++;
		  String right = expr.right.accept(this);
		  
		  //set the return type and the respective left and right type.
		  VisitorDecl vis = new VisitorDecl();
		  
		  String returnDef = expr.accept(vis);
		  
		  String nameDel = returnDef.replace("<", "").replace(">", "").replace(" ", "").replace(",", "");
		  
		  String leftDef = vis.leftSig().replace("this/", "");
		  String rightDef = vis.rightSig().replace("this/", "");
		  
		  if (leftDef.equals("Int")) {
			  leftDef = "int";
		  }
		  
		  if (rightDef.equals("Int")) {
			  rightDef = "int";
		  }
		  
		  if (expr.op == ExprBinary.Op.AND) {
			  out = "(" + left + " && " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.OR) {
			  out = "(" + left + " || " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.MUL) {
			  out = "(" + left + " * " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.DIV) {
			  out = "(" + left + " / " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.EQUALS) {
			  //==?
			  out = "(" + left + ".Equals(" + right + "))";
			  
		  } else if (expr.op == ExprBinary.Op.GT) {
			  out = "(" + left + " > " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.GTE) {
			  out = "(" + left + " >= " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.IFF) {
			  out = "(" + left + " == " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.IMINUS) {
			  out = "(" + left + " - " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.IMPLIES) {
			  out = "(!(" + left + ") || " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.IN) {
			  
//			  Version wo funkt, aber noch alt mit gebastel
//			  delsNeeded.add("  delegate bool BoolDel(" 
//					  + expr.left.toString().split("this/")[1].split("<:")[0].trim() 
//					  + " " + left.split("\\.")[0].replace("(", "") + ", " 
//					  + expr.right.toString().split("this/")[1].split("<:")[0].trim() 
//					  + " " + right.split("\\.")[0].replace("(", "") + ");\n");
//			  
//			  
//			  String tmp = "\tBoolDel delIn%1$d = (" + left.split("\\.")[0].replace("(", "") + ", " + right.split("\\.")[0].replace("(", "") + ") => {\n"
//					    + "\t" + String.format("\tvar l%d = %s;\n",i,left)
//					    + "\t" + String.format("\tvar r%d = %s;\n",i,right)
//					  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
//					  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
//					  	
//					  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
//					  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
//					  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
//					  	+ "\t\t} else {\n"
//					  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
//					  	+ "\t\t}\n"
//					  	
//					  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
//					  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
//					  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
//					  	+ "\t\t} else {\n"
//					  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
//					  	+ "\t\t}\n"
//					  	
//				  		+ "\t\treturn setLeft.IsSubsetOf(setRight);\n"
//				  		+ "\t};\n\n";
//			  
//			  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef);
//			  out = String.format("delIn%d(" + left.split("\\.")[0].replace("(", "") + ", " + right.split("\\.")[0].replace("(", "") + ")",i);
//			  
			  delsNeeded.add("  delegate bool BoolDel" + i + "(" 
					  + this.visit(expr.left.type()) 
					  + " a" + i + ", " 
					  + this.visit(expr.right.type())
					  + " b" + i + ");\n");
			  
			  
			  String tmp = "\tBoolDel" + i + " delIn%1$d = (a" + i + ", b" + i + ") => {\n"
					    + "\t" + String.format("\tvar l%d = %s;\n",i,"a" + i)
					    + "\t" + String.format("\tvar r%d = %s;\n",i,"b" + i)
					  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
					  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
					  	
					  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
					  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
					  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
					  	+ "\t\t}\n"
					  	
					  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
					  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
					  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
					  	+ "\t\t}\n"
					  	
				  		+ "\t\treturn setLeft.IsSubsetOf(setRight);\n"
				  		+ "\t};\n\n";
			  
			  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef);
			  out = String.format("delIn%d(" + left + ", " + right + ")",i);
			  
		  } else if (expr.op == ExprBinary.Op.INTERSECT) {
			  
			  delsNeeded.add("  delegate ISet<" + returnDef + "> SetDel" + nameDel + i + "(" 
					  + this.visit(expr.left.type())
					  + " a" + i + ", " 
					  + this.visit(expr.right.type())
					  + " b" + i + ");\n"); 
			  
			  
			  String tmp = "\tSetDel%6$s" + i + " delIntersect%1$d = (a" + i + ", b" + i + ") => {\n"
					    + "\t" + String.format("\tvar l%d = %s;\n",i,"a" + i)
					    + "\t" + String.format("\tvar r%d = %s;\n",i,"b" + i)
					  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
					  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"					  	
					  	
					  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
					  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
					  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
					  	+ "\t\t}\n"
					  	
					  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
					  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
					  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
					  	+ "\t\t}\n"
					  	
				  		+ "\t\tsetLeft.IntersectWith(setRight);\n"
				  		+ "\t\treturn setLeft;\n"

				  		+ "\t};\n\n";
			  
			  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef, nameDel);

			  out = String.format("delIntersect%d(" + left + ", " + right + ")",i);
			  
			  
		  } else if (expr.op == ExprBinary.Op.IPLUS) {
			  out = "(" + left + " + " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.JOIN) {
			  out = "(" + left +  "." + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.LT) {
			  out = "(" + left + " < " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.LTE) {
			  out = "(" + left + " =< " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.MINUS) {
			  if (returnDef.equals("int") && returnDef.equals("int") && returnDef.equals("int")) {
				  out = "(" + left + " - " + right + ")";
			  } else {
				  delsNeeded.add("  delegate ISet<" + returnDef + "> SetDel" + nameDel + i + "(" 
						  + this.visit(expr.left.type())
						  + " a" + i + ", "  
						  + this.visit(expr.right.type()) 
						  + " b" + i + ");\n"); 
				  
				  
				  String tmp = "\tSetDel%6$s" + i + " delMinus%1$d = (a" + i + ", b" + i + ") => {\n"
						  
						    + "\t" + String.format("\tvar l%d = %s;\n",i,"a" + i)
						    + "\t" + String.format("\tvar r%d = %s;\n",i,"b" + i)
						  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
						  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
						  	
						  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
						  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
						  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
						  	+ "\t\t} else {\n"
						  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
						  	+ "\t\t}\n"
						  	
						  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
						  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
						  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
						  	+ "\t\t} else {\n"
						  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
						  	+ "\t\t}\n"
						  	
					  		+ "\t\tsetLeft.ExceptWith(setRight);\n"
					  		+ "\t\treturn setLeft;\n"
	
					  		+ "\t};\n\n";
				  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef, nameDel);
				  out = String.format("delMinus%d(" + left + ", " + right + ")",i);
			  }
		  } else if (expr.op == ExprBinary.Op.NOT_EQUALS) {
			  out = "!(" + left + ".Equals( " + right + "))";
			  
		  } else if (expr.op == ExprBinary.Op.NOT_GT) {
			  out = "(" + left + " =< " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.NOT_GTE) {
			  out = "(" + left + " < " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.NOT_IN) {
			  
			  delsNeeded.add("  delegate bool BoolDel" + i + "(" 
					  + this.visit(expr.left.type()) 
					  + " a" + i + ", " 
					  + this.visit(expr.right.type())
					  + " b" + i + ");\n");
			  
			  
			  String tmp = "\tBoolDel" + i + " delNotIn%1$d = (a" + i + ", b" + i + ") => {\n"
					    + "\t" + String.format("\tvar l%d = %s;\n",i,"a" + i)
					    + "\t" + String.format("\tvar r%d = %s;\n",i,"b" + i)
					  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
					  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
					  	
					  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
					  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
					  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
					  	+ "\t\t}\n"
					  	
					  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
					  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
					  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
					  	+ "\t\t}\n"
					  	
				  		+ "\t\treturn !(setLeft.IsSubsetOf(setRight));\n"
				  		+ "\t};\n\n";
			  
			  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef);
			  out = String.format("delNotIn%d(" + left + ", " + right + ")",i);
			  
		  } else if (expr.op == ExprBinary.Op.NOT_LT) {
			  out = "(" + left + " >= " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.NOT_LTE) {
			  out = "(" + left + " > " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.PLUS) {
			  if (returnDef.equals("int") && returnDef.equals("int") && returnDef.equals("int")) {
				  out = "(" + left + " - " + right + ")";
			  } else {
				  delsNeeded.add("  delegate ISet<" + returnDef + "> SetDel" + nameDel + i +"(" 
						  + this.visit(expr.left.type())
						  + " a" + i + ", "  
						  + this.visit(expr.right.type()) 
						  + " b" + i + ");\n");
				  
				  				  				  
				  String tmp = "\tSetDel%6$s" + i + " delPlus%1$d = (a" + i + ", b" + i + ") => {\n"
						  
						    + "\t" + String.format("\tvar l%d = %s;\n",i,"a" + i)
						    + "\t" + String.format("\tvar r%d = %s;\n",i,"b" + i)
						  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
						  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
						  	
						  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
						  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
						  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
						  	+ "\t\t} else {\n"
						  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
						  	+ "\t\t}\n"
						  	
						  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
						  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
						  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
						  	+ "\t\t} else {\n"
						  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
						  	+ "\t\t}\n"
						  
					  		+ "\t\tsetLeft.UnionWith(setRight);\n"
					  		+ "\t\treturn setLeft;\n"
					  		
					  		+ "\t};\n\n";
				  
				  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef, nameDel);
	
				  out = String.format("delPlus%d(" + left + ", " + right + ")",i);
			  }
		  } else if (expr.op == ExprBinary.Op.REM) {
			  out = "(" + left + " % " + right + ")";
			  
		  } else {
			  out = "(" + left + expr.op + right + ")";
			  
		  }
		  return out;
		  
	}
}








/* Version vom 14.4, bei 12/12. wobei z.B. 'in' nicht unterstuetzt wird usw
package edu.mit.csail.sdg.alloy4compiler.generator;

import java.util.ArrayList;

import edu.mit.csail.sdg.alloy4compiler.ast.Type;
import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprBinary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprCall;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprConstant;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprQt;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprUnary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprVar;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.ast.VisitQuery;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.Field;

public class TestVisitor extends VisitQuery<String> {
	
	//the Sets are this type.
		public String defs = "";
		
		//the delegates I need. Just Copy from here.
		public ArrayList<String> delsNeeded= new ArrayList<String>();
		
		int i = 0;
	
	
	private String objectType; 

	@Override
	public String visit(Sig sig) throws Err {
		return sig.label.replace("this/", "").replace("seq/", "").replace("boolean/", "");
	}

	@Override
	public String visit(Field f) throws Err {
		return f.label;
	}

	public String visit(Type typ) throws Err {
		return typ.toString().substring(6).replace("}", "").replace("an/", "");
	}

	@Override
	public String visit(ExprVar expr) throws Err {		
		return expr.label.replace("$", "").replace("boolean/", "");
	}
			
	public String clean(String s) throws Err {
		s = s.replace("$", "");		
		return s;
	}	
	
	@Override public String visit(ExprCall x) throws Err {
    	
    	// The answer-string
    	String ans = "";
    	
    	ans = "FuncClass." + x.fun.label.substring(5) + "(";
    	int s = x.args.size();
    	if (s > 1) {
    		for (int i = 0; i < s-1; i++) {
    			ans = ans + x.args.get(i).accept(this) + ", ";
    		}
    		ans = ans + x.args.get(s-1).accept(this) + ")";
    	} else if (s == 1) {
    		ans = ans + x.args.get(0).accept(this) + ")";
    	} else {
    		ans = ans + ")";
    	}
    	return ans;    	
    }	
	
	@Override
	public String visit(ExprConstant expr) throws Err {
		if(expr.op == ExprConstant.Op.FALSE){
			return "false";
		} else if (expr.op == ExprConstant.Op.TRUE) {
			return "true";
		} else if (expr.op == ExprConstant.Op.MAX) {
			return "Int.MaxValue";
		} else if (expr.op == ExprConstant.Op.MIN) {
			return "Int.MinValue";
		} else if (expr.op == ExprConstant.Op.NUMBER) {
			return expr.num + "";
		} else if (expr.op == ExprConstant.Op.STRING) {
			return expr.string;
		} else {
			return expr.op.toString();
		}
	}
	
	@Override
	public String visit(ExprUnary expr) throws Err {
		int i = this.i;
		
		this.i++;
		String subString = expr.sub.accept(this);
		
		if (expr.op == ExprUnary.Op.CLOSURE) {
			return "Helper.Closure(" + subString + ")";
			
		} else if (expr.op == ExprUnary.Op.RCLOSURE) {
			return "Helper.RClosure(" + subString + ")";
			
		} else if (expr.op == ExprUnary.Op.NOT) {
			return "!" + subString;
			
		} else if (expr.op == ExprUnary.Op.CARDINALITY) {
			  defs += String.format("\tvar sub%d = %s;\n",i, subString);

			  String tmp = "\tIntDel delCardinality%1$d = () => {\n"
				  		+ "\t\tif (sub%1$d is ISet<%2$s>)\n"
				  		+ "\t\t\treturn ((ISet<%2$s>) sub%1$d).Count();\n"
				  		+ "\t\telse\n"
				  		+ "\t\t\treturn 1;\n"
				  		+ "\t};\n\n";
			  
			  defs += String.format(tmp, i, objectType);
			  return String.format("delCardinality%d()",i);
			
		} else if (expr.op == ExprUnary.Op.CAST2INT) {
			return "CAST2INT " + subString;
			
		} else if (expr.op == ExprUnary.Op.CAST2SIGINT) {
			return "CAST2SIGINT " + subString;
			
		} else if (expr.op == ExprUnary.Op.LONEOF) {
			return subString;
			
		} else if (expr.op == ExprUnary.Op.ONEOF) {
			return subString;
			
		} else if (expr.op == ExprUnary.Op.SETOF) {
			return "ISet< "+ subString + ">";
			
		} else if (expr.op == ExprUnary.Op.SOMEOF) {
			return "ISet< "+ subString + ">";
			
		} else if (expr.op == ExprUnary.Op.TRANSPOSE) {
			return "TRANSPOSE " + subString;
			
		}
		
		return subString;
	}
	
    @Override
    public String visit(ExprQt x) throws Err {
    	
    	// The answer-string
    	String ans = "";
    	
    	// Quantifiers    	
    	if (x.op == ExprQt.Op.ALL) {
    		ans = ans + "Contract.ForAll(" + x.decls.get(0).expr.accept(this) + "Set, " + x.decls.get(0).names.get(0).accept(this) + " => "; 
    		ans = ans + x.sub.accept(this);
    		ans = ans + ")";
    	}
    	else if (x.op == ExprQt.Op.LONE) {
    		ans = ans + x.decls.get(0).expr.accept(this) + "Set.Where(" + x.decls.get(0).names.get(0).accept(this) + " => ";
    		ans = ans + x.sub.accept(this);
    		ans = ans + ").Count() == 0 || " + x.decls.get(0).expr.accept(this) + "Set.Where(" + x.decls.get(0).names.get(0).accept(this) + " => ";
    		ans = ans + x.sub.accept(this);
    		ans = ans + ").Count() == 1";
    	}
    	else if (x.op == ExprQt.Op.NO) {
    		ans = ans + "Contract.ForAll(" + x.decls.get(0).expr.accept(this) + "Set, " + x.decls.get(0).names.get(0).accept(this) + " => !";
    		ans = ans + x.sub.accept(this);
			ans = ans + ")";
		} 
    	else if (x.op == ExprQt.Op.ONE) {
    		ans = ans + x.decls.get(0).expr.accept(this) + "Set.Where(" + x.decls.get(0).names.get(0).accept(this) + " => ";
    		ans = ans + x.sub.accept(this);
    		ans = ans + ").Count() == 1";
    	}
    	else if (x.op == ExprQt.Op.SOME) {
    		ans = ans + "Contract.Exists(" + x.decls.get(0).expr.accept(this) + "Set, " + x.decls.get(0).names.get(0).accept(this) + " => ";
    		ans = ans + x.sub.accept(this);
			ans = ans + ")";
		}
    	else {
    		// Quantifier we don't have to support!
    		ans = ans + "Not supported quantifier in ExprQt-Visitor";
    	}
        
        return ans;
    }
    
    @Override
	public String visit(ExprBinary expr) throws Err {
		  String out;		 
		  int i = this.i;
		 
		  this.i++;
		  String left = expr.left.accept(this);
		  
		  this.i++;
		  String right = expr.right.accept(this);
		  
		  //set the return type and the respective left and right type.
		  VisitorDecl vis = new VisitorDecl();
		  
		  String returnDef = expr.accept(vis);
		  
		  String nameDel = returnDef.replace("<", "").replace(">", "").replace(" ", "").replace(",", "");
		  
		  String leftDef = vis.leftSig().replace("this/", "");
		  String rightDef = vis.rightSig().replace("this/", "");
		  
		  if (leftDef.equals("Int")) {
			  leftDef = "int";
		  }
		  
		  if (rightDef.equals("Int")) {
			  rightDef = "int";
		  }
		  
		  if (expr.op == ExprBinary.Op.AND) {
			  out = "(" + left + " && " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.OR) {
			  out = "(" + left + " || " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.MUL) {
			  out = "(" + left + " * " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.DIV) {
			  out = "(" + left + " / " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.EQUALS) {
			  //==?
			  out = "(" + left + ".Equals(" + right + "))";
			  
		  } else if (expr.op == ExprBinary.Op.GT) {
			  out = "(" + left + " > " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.GTE) {
			  out = "(" + left + " >= " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.IFF) {
			  out = "(" + left + " == " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.IMINUS) {
			  out = "(" + left + " - " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.IMPLIES) {
			  out = "(!(" + left + ") || " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.IN) {
			  defs += String.format("\tvar l%d = %s;\n",i,left);
			  defs += String.format("\tvar r%d = %s;\n",i,right);

			  String tmp = "\tBoolDel delIn%1$d = () => {\n"
					  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
					  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
					  	
					  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
					  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
					  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
					  	+ "\t\t}\n"
					  	
					  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
					  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
					  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
					  	+ "\t\t}\n"
					  	
				  		+ "\t\treturn setLeft.IsSubsetOf(setRight);\n"
				  		+ "\t};\n\n";
			  
			  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef);
			  out = String.format("delIn%d()",i);
			  
		  } else if (expr.op == ExprBinary.Op.INTERSECT) {
			  delsNeeded.add("  delegate ISet<" + returnDef + "> SetDel" + nameDel + "();\n");
			  
			  defs += String.format("\tvar l%d = %s;\n",i,left);
			  defs += String.format("\tvar r%d = %s;\n",i,right);

			  String tmp = "\tSetDel%6$s delIntersect%1$d = () => {\n"
					  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
					  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
					  	
					  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
					  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
					  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
					  	+ "\t\t}\n"
					  	
					  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
					  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
					  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
					  	+ "\t\t}\n"
					  	
				  		+ "\t\tsetLeft.IntersectWith(setRight);\n"
				  		+ "\t\treturn setLeft;\n"

				  		+ "\t};\n\n";
			  
			  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef, nameDel);

			  out = String.format("delIntersect%d()",i);
		  } else if (expr.op == ExprBinary.Op.IPLUS) {
			  out = "(" + left + " + " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.JOIN) {
			  out = "(" + left +  "." + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.LT) {
			  out = "(" + left + " < " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.LTE) {
			  out = "(" + left + " =< " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.MINUS) {
			  if (returnDef.equals("int") && returnDef.equals("int") && returnDef.equals("int")) {
				  out = "(" + left + " - " + right + ")";
			  } else {
				  delsNeeded.add("  delegate ISet<" + returnDef + "> SetDel" + nameDel + "();\n");
				  
				  defs += String.format("\tvar l%d = %s;\n",i,left);
				  defs += String.format("\tvar r%d = %s;\n",i,right);
				  String tmp = "\tSetDel%6$s delMinus%1$d = () => {\n"
						  
						  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
						  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
						  	
						  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
						  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
						  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
						  	+ "\t\t} else {\n"
						  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
						  	+ "\t\t}\n"
						  	
						  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
						  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
						  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
						  	+ "\t\t} else {\n"
						  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
						  	+ "\t\t}\n"
						  	
					  		+ "\t\tsetLeft.ExceptWith(setRight);\n"
					  		+ "\t\treturn setLeft;\n"
	
					  		+ "\t};\n\n";
				  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef, nameDel);
				  out = String.format("delMinus%d()",i);
			  }
		  } else if (expr.op == ExprBinary.Op.NOT_EQUALS) {
			  out = "!(" + left + ".Equals( " + right + "))";
			  
		  } else if (expr.op == ExprBinary.Op.NOT_GT) {
			  out = "(" + left + " =< " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.NOT_GTE) {
			  out = "(" + left + " < " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.NOT_IN) {
			  defs += String.format("\tvar l%d = %s;\n",i,left);
			  defs += String.format("\tvar r%d = %s;\n",i,right);

			  String tmp = "\tBoolDel delIn%1$d = () => {\n"
					  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
					  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
					  	
					  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
					  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
					  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
					  	+ "\t\t}\n"
					  	
					  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
					  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
					  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
					  	+ "\t\t} else {\n"
					  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
					  	+ "\t\t}\n"
					  	
				  		+ "\t\treturn !(setLeft.IsSubsetOf(setRight));\n"
				  		+ "\t};\n\n";
			  
			  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef);
			  out = String.format("delNotIn%d()",i);
			  
		  } else if (expr.op == ExprBinary.Op.NOT_LT) {
			  out = "(" + left + " >= " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.NOT_LTE) {
			  out = "(" + left + " > " + right + ")";
			  
		  } else if (expr.op == ExprBinary.Op.PLUS) {
			  if (returnDef.equals("int") && returnDef.equals("int") && returnDef.equals("int")) {
				  out = "(" + left + " - " + right + ")";
			  } else {
				  delsNeeded.add("  delegate ISet<" + returnDef + "> SetDel" + nameDel + "();\n");
				  
				  defs += String.format("\tvar l%d = %s;\n",i,left);
				  defs += String.format("\tvar r%d = %s;\n",i,right);
	
				  String tmp = "\tSetDel%6$s delPlus%1$d = () => {\n"
						  
						  	+ "\t\tISet<%3$s> setLeft = new HashSet<%3$s>();\n"
						  	+ "\t\tISet<%3$s> setRight = new HashSet<%3$s>();\n"
						  	
						  	+ "\t\tif (l%1$d is ISet<%4$s>) {\n"
						  	+ "\t\t\tforeach (%4$s el in (ISet<%4$s>) l%1$d)\n"
						  	+ "\t\t\t\tsetLeft.Add((%3$s) el);\n"
						  	+ "\t\t} else {\n"
						  	+ "\t\t\tsetLeft.Add((%3$s) l%1$d);\n"
						  	+ "\t\t}\n"
						  	
						  	+ "\t\tif (r%1$d is ISet<%5$s>) {\n"
						  	+ "\t\t\tforeach (%5$s el in (ISet<%5$s>) r%1$d)\n"
						  	+ "\t\t\t\tsetRight.Add((%3$s) el);\n"
						  	+ "\t\t} else {\n"
						  	+ "\t\t\tsetRight.Add((%3$s) r%1$d);\n"
						  	+ "\t\t}\n"
						  
					  		+ "\t\t(setLeft.UnionWith(setRight));\n"
					  		+ "\t\treturn setLeft;\n"
					  		
					  		+ "\t};\n\n";
				  
				  defs += String.format( tmp , i, "HashSet<" + returnDef + ">", returnDef, leftDef, rightDef, nameDel);
	
				  out = String.format("delPlus%d()",i);
			  }
		  } else if (expr.op == ExprBinary.Op.REM) {
			  out = "(" + left + " % " + right + ")";
			  
		  } else {
			  out = "(" + left + expr.op + right + ")";
			  
		  }
		  return out;
		  
	}

}
*/