package edu.mit.csail.sdg.alloy4compiler.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.util.Iterator;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4.ErrorFatal;
import edu.mit.csail.sdg.alloy4.SafeList;
import edu.mit.csail.sdg.alloy4compiler.ast.Expr;
import edu.mit.csail.sdg.alloy4compiler.ast.Func;
import edu.mit.csail.sdg.alloy4compiler.ast.Module;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.Field;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.PrimSig;

public final class CodeGenerator {

	
  private String otherDelegates = "";
  private boolean containsClosure = false;;
  private boolean containsRClosure = false;
  
  private CodeGenerator(Iterable<Sig> sigs, SafeList<Func> funcs, String originalFilename, PrintWriter out, boolean checkContracts) throws Err {
        
        if (!checkContracts)
            out.append("#undef CONTRACTS_FULL\n\n");
        
	  //add the top lines from the provided C# file.
	out.append("using System;\nusing System.Linq;\nusing System.Collections.Generic;\nusing System.Diagnostics.Contracts;\n\n");
	
	Iterator<Sig> sigIter = sigs.iterator();
	while (sigIter.hasNext()) {
	    Sig nextSig = sigIter.next();
	    if (!nextSig.builtin)
		    out.append(sigToCSharp(nextSig));
	}
	
	//FuncClass as in answer0.cs
	Iterator<Func> fIter = funcs.iterator();
	boolean hasFuncs = false;
	if (fIter.hasNext()) {
		out.append("public static class FuncClass {\n\n");
		otherDelegates += "  delegate ISet<Object> SetDelObject();\n";
		otherDelegates += "  delegate bool BoolDel();\n";
		otherDelegates += "  delegate int IntDel();\n";
		hasFuncs = true;
	}
	
	while (fIter.hasNext()) {
		Func f = fIter.next();
		String func = funcToCSharp(f);
		
		if (func.contains("Closure"))
			containsClosure = true;
		
		if (func.contains("RClosure"))
			containsRClosure = true;
		
		out.append(func);
	}
	
	if (hasFuncs) {
		out.append(otherDelegates);
		out.append("}\n");
	}
	
	if (containsClosure) {
		
		//FuncClass
		out.append("public static class Helper {\n"
				+ "\tpublic static ISet<Tuple<L, R>> Closure<L, R>(ISet<Tuple<L, R>> set) {\n"
				+ "\t\tIEnumerator<Tuple<L,R>> en1 = set.GetEnumerator();\n"
				+ "\t\tTuple<L, R> tuple = en1.Current;\n"
				+ "\t\tif (en1.Current.Item1 is R)\n"
				+ "\t\t{\n"
				+ "\t\t\tint oldCount;\n"
				+ "\t\t\tdo\n"
				+ "\t\t\t{\n"
				+ "\t\t\t\toldCount = set.Count;\n"
				+ "\t\t\t\ten1 = set.GetEnumerator();\n"
				+ "\t\t\t\tdo\n"
				+ "\t\t\t\t{\n"
				+ "\t\t\t\t\tR conn = en1.Current.Item2;\n"
				+ "\t\t\t\t\tL start = en1.Current.Item1;\n"
				+ "\t\t\t\t\tIEnumerator<Tuple<L, R>> en2 = set.GetEnumerator();\n"
				+ "\t\t\t\t\tdo\n"
				+ "\t\t\t\t\t{\n"
				+ "\t\t\t\t\t\tif (en2.Current.Item1.Equals(conn))\n"
				+ "\t\t\t\t\t\t{\n"
				+ "\t\t\t\t\t\t\tTuple<L, R> t = new Tuple<L, R>(start, en2.Current.Item2);\n"
				+ "\t\t\t\t\t\t\tset.Add(t);\n"
				+ "\t\t\t\t\t\t}\n"
				+ "\t\t\t\t\t} while (en2.MoveNext());\n"
				+ "\t\t\t\t} while (en1.MoveNext());\n"
				+ "\t\t\t} while (oldCount < set.Count);\n"
				+ "\t\t}\n"
				+ "\t\treturn set;\n"
				+ "\t}\n");
		
		if (containsRClosure)
			out.append("\tpublic static ISet<Tuple<L, R>> RClosure<L, R>(ISet<Tuple<L, R>> set) {\n"
					+ "\t\tset = Closure(set);\n"
					+ "\t\tISet<Tuple<L, R>> refl = new HashSet<Tuple<L,R>>();\n"
					+ "\t\tforeach (Tuple<L,R> t in set) {\n"
					+ "\t\t\tif (t.Item1 is R)\n"
					+ "\t\t\t\trefl.Add(new Tuple<L, R>(t.Item1,(dynamic) t.Item1));\n"
					+ "\t\t}\n"
					+ "\t\tset.UnionWith(refl);\n"
					+ "\t\treturn set;\n"
					+ "\t}\n"
					+ "}\n");
		else
			out.append("}\n");
	}
	out.close();
  }

  private String sigToCSharp(Sig sig) throws Err {
	  //returns the name from the 'sig'. Removes the 'this/' part from the string.
	  String identifier = sig.label.substring(5);
	  
	  //the string which will eventually represent 'sig'.
	  String s = "";
	  
	  //contains the sig from which it is a descendant. Never contains 'UNIV'.
	  String superSig = "";
	  
	  //the string which will contain all Object invariants.
	  String invar = "\n" +
                     "  [ContractInvariantMethod]\n" +
                     "  private void ObjectInvariant() {\n";
	  
	  //is the class abstract?
	  if (sig.isAbstract != null)
	    s += "abstract ";
	  
	  
	  //set up the header.
	  s += "public class " + identifier + " ";
	  
	  //Inheritance
	  if (!sig.isTopLevel()) {
		  PrimSig pSig = (PrimSig) sig;
		  superSig = pSig.parent.label.substring(5);
		  s += ": " + superSig + " ";
	  }
	  
	  //end Header
	  s += "{\n";
	  
	  //support one in 'sig' definition. singleton implementation as in answer0.cs
	  if (sig.isOne != null) {
		  s += "  private static " + identifier + " instance;\n" +
			   "  private " + identifier + "() { }\n" + 
			   "  public static " + identifier + " Instance {\n" +
			   "    get {\n" + 
			   "      if (instance == null) {\n" + 
			   "        instance = new " + identifier + "();\n" +
			   "      }\n" + 
			   "      return instance;\n" +
			   "    }\n" +
			   "  }\n";
	  }
	  
	  for(Field field:sig.getFields()) {
		  //the type.
		  String decl = field.decl().expr.accept(new VisitorDecl());
		  
		  //set the field
		  s += "  public " + decl + " " + field.label + ";\n";
		  

		  //contains the dependencies to other fields.
		  Expr deps = field.decl().expr;
		  
		  String addConst = deps.accept(new VisitorInv());
		  
		  if (!addConst.isEmpty())
			  invar += "    Contract.Invariant(" + field.label + " != null && " + String.format(addConst, field.label) + ");\n";
		  else
			  invar += "    Contract.Invariant(" + field.label + " != null);\n";
	  }

	  
	  //Attributes are of no specific interest.
	  
	  //close the object invariant.
	  invar += "  }\n";
	  
	  if (!sig.getFields().isEmpty())
		  s += invar;
	  
	  //close the class.
	  s += "}\n\n";
	  
	  return s;
  }
  
  private String funcToCSharp(Func f) throws Err{
	  //Just for prettyPrinting.
	  String ident = "  ";
	  //the features eventual string. will be returned.
	  String s = ident + "public static ";
	  //the preconditions
	  String prec = "";
	  //the postconditions
	  String post = "";
	  //the return type
	  String retDecl = "bool";
	  //to look at the statement.
	  Visitor bodyVis;
	  //the body in C#
	  String bodyC = "";
	  
	  if (f.isPred) {
		  //the function is a Predicate, therefore it returns a bool.
		  s += "bool ";
	  } else {
		  retDecl = f.returnDecl.accept(new VisitorDecl());;
		  
	      //add the return type.
	      s += retDecl + " ";
	      
	      //set the postconditions
	      Expr deps = f.returnDecl;
	      
		  String addConst = deps.accept(new VisitorInv());
		  
		  if (!addConst.isEmpty())
			  post += "    Contract.Ensures(Contract.Result<" + retDecl + ">() != null && " + String.format(addConst, "Contract.Result<" + retDecl + ">()") + ");\n";
		  else
			  post += "    Contract.Ensures(Contract.Result<" + retDecl + ">() != null);\n";
	  }
	  
	  //the feature's name.
	  s += f.label.substring(5) + "(";
	  
	  for (int i=0; i < f.decls.size(); i++) {
		  
		  String type = f.decls.get(i).expr.accept(new VisitorDecl());
		  
		  //add the type and the instance's name.
		  for (int k = 0; k < f.decls.get(i).names.size(); k++) {
			  s += type + " " + f.decls.get(i).names.get(k);
			  if (k < f.decls.get(i).names.size() - 1)
				  s += ", ";
		  }
		  
		  if (i < f.decls.size() - 1)
			  s += ", ";
		  
		  //set the precondition
		  for (int k = 0; k < f.decls.get(i).names.size(); k++) {
			  Expr deps = f.decls.get(i).expr;
			  
			  String addConst = deps.accept(new VisitorInv());
			  
			  if (!addConst.isEmpty())
				  prec += "    Contract.Requires(" + f.decls.get(i).names.get(k) + " != null && " + String.format(addConst, f.decls.get(i).names.get(k)) + ");\n";
			  else
				  prec += "    Contract.Requires(" + f.decls.get(i).names.get(k) + " != null);\n";

		  }
		  
	  }
	  
	  //close the declaration - header.
	  s += ") {\n";
	  
	  //add the precondition and postcondition.
	  s += prec + "\n";
	  
	  s += post + "\n";
	  
	  //set the visitor
	  bodyVis = new Visitor();
	  
	  //set the body
	  bodyC = f.getBody().accept(bodyVis);
	  
	  //add the necessary definitions for the computation of the statement
	  s += bodyVis.defs;
	  
	  //add the body to the return statement
	  if (!retDecl.contains("ISet") && !retDecl.equals("int") && !retDecl.equals("bool"))
		  s += "    if (" + bodyC + " is ISet<" + retDecl + ">) return ((ISet<" + retDecl + ">) " + bodyC + ").Single(); else return ((" + retDecl + ") " + bodyC +");";
	  else
		  s += "    return ((" + retDecl + ") " + bodyC + ");\n";
	  
	  s += ident + "}\n";
	  
	  for (String del:bodyVis.delsNeeded) {
		  if (!otherDelegates.contains(del)) {
			  otherDelegates += del;
		  }
	  }
	  
	  return s + "\n";
  }
  
  public static void writeCode(Module world, String originalFilename, boolean checkContracts, boolean saveInDist) throws Err {
    try {
      String f;
      String ext = ".cs";
      if (saveInDist) {
        f = ".\\" + new File(originalFilename).getName() + ext;
        //System.out.println(f);
      }
      else {
        f = originalFilename + ext;
      }
      File file = new File(f);
      if (file.exists()) {
        file.delete();
      }
      PrintWriter out = new PrintWriter(new FileWriter(f, true));
      new CodeGenerator(world.getAllReachableSigs(), world.getAllFunc(), originalFilename, out, checkContracts);
    }
    catch (Throwable ex) {
    	ex.printStackTrace();
      if (ex instanceof Err) {
        throw (Err)ex;
      }
      else {
        throw new ErrorFatal("Error writing the generated C# code file.", ex);
      }
    }
  }
}
