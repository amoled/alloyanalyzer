package edu.mit.csail.sdg.alloy4compiler.generator;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;

import edu.mit.csail.sdg.alloy4.Err;
import edu.mit.csail.sdg.alloy4.ErrorFatal;
import edu.mit.csail.sdg.alloy4.Pair;
import edu.mit.csail.sdg.alloy4compiler.ast.Command;
import edu.mit.csail.sdg.alloy4compiler.ast.Expr;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprUnary;
import edu.mit.csail.sdg.alloy4compiler.ast.ExprVar;
import edu.mit.csail.sdg.alloy4compiler.ast.Module;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig;
import edu.mit.csail.sdg.alloy4compiler.ast.Sig.Field;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Solution;
import edu.mit.csail.sdg.alloy4compiler.translator.A4Tuple;


public final class TestGenerator {
	
  private String otherDelegates = "";	  

  private TestGenerator(A4Solution solution, Iterable<Sig> sigs, Iterable<Pair<String, Expr>> assertions, Iterable<Command> cmds, String originalFilename, PrintWriter out) throws Err {
	  	  
	  // first commented line
	  out.append("// This C# file is generated from " + originalFilename + "\n\n");
	  
	  // using import statements
	  out.append("using System;\n"
	  		+ "using System.Linq;\n"
	  		+ "using System.Collections.Generic;\n"
	  		+ "using System.Diagnostics.Contracts;\n\n");
	  
	  // class Test and Main - open
	  out.append("public static class Test {\n"
	  		+ "  public static void Main(string[] args) {\n"
	  		+ "    // setup test data\n");
	  
	  // if counterexample is found, create C# object structures
	  if (solution.satisfiable()) { // I'm not sure if satisfiable really means that there's a counterexample..
		  Iterable<ExprVar> atomList = solution.getAllAtoms(); // atoms are the elements in the diagrams, e.g. [Platform$0, Man$0, Man$1, Eve$0]		  
		  		  
		  // THE VISITOR
		  TestVisitor v = new TestVisitor();

		  String sigl = ""; // all one-sigs
		  for (Sig s : sigs) {
			  if (s.isOne != null) {

				  sigl = sigl.concat(v.visit(s));
			  }
		  }
		  		  
		  // create set for each signature and add the corresponding elements to it, and link the fields
		  for (Sig s : sigs) { 
			  
			  // cover only non-builtin sigs
			  if (!s.builtin) {				  			 		  
				  
				  // create set	
				  out.append("    var " + v.visit(s) + "Set = new HashSet<" + v.visit(s) + ">();\n");	
				  				  
				  // create instances and add elements to the set for every atom
				  for (ExprVar e : atomList) {
					  
					  // elements that are related somehow
					  if (e.type().isSubtypeOf(s.type())) { // e.toString() == Man$1, s.toString() == this/Man	
						  
						  String backupStr = "";
						  String thisline = "";
							  
						  // top-level-sig
						  if (s.isTopLevel()) {
							  
							  // Declare instance for every atom
							  out.append("    " + v.visit(e.type()) + " " + v.visit(e) + ";\n");
							  
							  // elements that are of the equivalent type
							  if (e.type() == s.type()) {	
								  if (s.isOne != null) { // is one-multiplicity	
									  thisline = "    " + v.visit(s) + "Set.Add(" + v.visit(e) + " = " + v.visit(s) + ".Instance);\n";
									  out.append(thisline);
									  backupStr = backupStr.concat(thisline);
								  } else {
									  thisline = "    " + v.visit(s) + "Set.Add(" + v.visit(e) + " = new " + v.visit(s) + "());\n";
									  out.append(thisline);
									  backupStr = backupStr.concat(thisline);
								  }
							  }
							  
						  // not top-level							  
						  } else {
							  out.append("    " + v.visit(s) + "Set.Add(" + v.visit(e) + ");\n");							  							  
						  }		
						  
						// abstract sig -- add elements of override-sigs to the set
						  if (s.isAbstract != null) { 
							  if (sigl.contains(v.visit(e.type()))) { // that means e is instance of one-sig
								  thisline = "    " + v.visit(s) + "Set.Add(" + v.visit(e) + " = " + v.visit(e.type()) + ".Instance);\n";
								  if (!backupStr.contains(thisline)) {
									  out.append(thisline);
								  }									  
								  backupStr = backupStr.concat(thisline);
							  } else {
								  thisline = "    " + v.visit(s) + "Set.Add(" + v.visit(e) + " = new " + v.visit(e.type()) + "());\n";
								  if (!backupStr.contains(thisline)) {
									  out.append(thisline);
								  }
								  backupStr = backupStr.concat(thisline);
							  }
							  						  
						  }
					  }
				  }				 
			  
				  // add field linkings for every field
				  for (Field f : s.getFields()) {
					  String backupHash = "";
					  for (A4Tuple a : solution.eval(f)) {
						  if (a.arity() == 2) { 
							  String swica = f.accept(new TestVisitorFields());
							  if (swica.equals("oneof") || swica.equals("loneof")) { 
								  out.append("    " + v.clean(a.atom(0)) + "." + f.label + " = " + v.clean(a.atom(1)) + ";\n");
							  } else { // zz setof and someof
								  String declHash = "    " + v.clean(a.atom(0)) + "." + f.label + " = new HashSet<" + v.visit(a.sig(1)) + ">();\n";
								  if (!backupHash.contains(declHash)) { // if not already declared HashSet declare it
									  out.append(declHash);
									  backupHash = backupHash.concat(declHash);
								  }
								  out.append("    " + v.clean(a.atom(0)) + "." + f.label + ".Add(" + v.clean(a.atom(1)) + ");\n");
							  }
						  } else if (a.arity() == 3) {
							  String declHash = "    " + v.clean(a.atom(0)) + "." + f.label + " = new HashSet<Tuple<" + v.visit(a.sig(1)) + ", " + v.visit(a.sig(2)) + ">>();\n";
							  if (!backupHash.contains(declHash)) { // if not already declared the HashSet declare it
								  out.append(declHash);
								  backupHash = backupHash.concat(declHash);
							  }
							  out.append("    " + v.clean(a.atom(0)) + "." + f.label + ".Add(Tuple.Create(" + v.clean(a.atom(1)) + ", " + v.clean(a.atom(2)) + "));\n");
						  }
					  }					  
				  }
			  }			  
		  }			  
	  }	    
	  

	  TestVisitor v = new TestVisitor();
	  String s = "";
 
	  if (solution.satisfiable()) {
		// Contracts
		  out.append("\n    // check test data\n");
		  
		  // iterates over every assertion
		  for (Pair<String, Expr> ass : assertions) {
//			  out.append("    Contract.Assert(");
			  s += "    Contract.Assert(";
			  
			  // visit every assertion
//			  out.append(v.visit((ExprUnary) ass.b));
			  s += v.visit((ExprUnary) ass.b);
			  
			  // append name of assertion
//			  out.append(", \"" + ass.a + "\"");
			  s += ", \"" + ass.a + "\"";
			  
			  // close assert
//			  out.append(");\n");
			  s += ");\n";
		  }	
	  }
	   
	  

	  otherDelegates += "  delegate ISet<Object> SetDelObject();\n";
	  otherDelegates += "  delegate bool BoolDel();\n";
	  otherDelegates += "  delegate int IntDel();\n";

	  //add the necessary definitions for the computation of the statement
	  out.append(v.defs);
	  
	  for (String del:v.delsNeeded) {
		  if (!otherDelegates.contains(del)) {
			  otherDelegates += del;
		  }
	  }

		
	  out.append(s);
	  
	  // class Test and Main - close
	  out.append("  }\n");
	  
	  out.append("\n" + otherDelegates + "\n");
	  
  	  out.append("}\n");
	  out.close();
	  
  }
  
  public static void writeTest(A4Solution solution, Module world, String originalFilename, boolean saveInDist) throws Err {
    try {
      String f;
      String ext = ".tests.cs";
      if (saveInDist) {
        f = ".\\" + new File(originalFilename).getName() + ext;
      }
      else {
        f = originalFilename + ext;
      }
      File file = new File(f);
      if (file.exists()) {
        file.delete();
      }
      PrintWriter out = new PrintWriter(new FileWriter(f, true));
      new TestGenerator(solution, world.getAllReachableSigs(), world.getAllAssertions(), world.getAllCommands(), originalFilename, out);
    }
    catch (Throwable ex) {
      if (ex instanceof Err) {
        throw (Err)ex;
      }
      else {
        throw new ErrorFatal("Error writing the generated C# test file.", ex);
      }
    }
  }
}
